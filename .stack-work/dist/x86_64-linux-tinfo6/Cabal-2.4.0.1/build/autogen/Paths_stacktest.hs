{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_stacktest (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/gaurdianaq/Speedy/Projects/gaurdianaq-portfolio/.stack-work/install/x86_64-linux-tinfo6/7b0d1666fd5fd3d1c51c5b7c84923da3f6efa3d802d29dd2918319cf8a902dff/8.6.5/bin"
libdir     = "/home/gaurdianaq/Speedy/Projects/gaurdianaq-portfolio/.stack-work/install/x86_64-linux-tinfo6/7b0d1666fd5fd3d1c51c5b7c84923da3f6efa3d802d29dd2918319cf8a902dff/8.6.5/lib/x86_64-linux-ghc-8.6.5/stacktest-0.1.0.0-4k3fz1J4l7hDBtSFlh20wC"
dynlibdir  = "/home/gaurdianaq/Speedy/Projects/gaurdianaq-portfolio/.stack-work/install/x86_64-linux-tinfo6/7b0d1666fd5fd3d1c51c5b7c84923da3f6efa3d802d29dd2918319cf8a902dff/8.6.5/lib/x86_64-linux-ghc-8.6.5"
datadir    = "/home/gaurdianaq/Speedy/Projects/gaurdianaq-portfolio/.stack-work/install/x86_64-linux-tinfo6/7b0d1666fd5fd3d1c51c5b7c84923da3f6efa3d802d29dd2918319cf8a902dff/8.6.5/share/x86_64-linux-ghc-8.6.5/stacktest-0.1.0.0"
libexecdir = "/home/gaurdianaq/Speedy/Projects/gaurdianaq-portfolio/.stack-work/install/x86_64-linux-tinfo6/7b0d1666fd5fd3d1c51c5b7c84923da3f6efa3d802d29dd2918319cf8a902dff/8.6.5/libexec/x86_64-linux-ghc-8.6.5/stacktest-0.1.0.0"
sysconfdir = "/home/gaurdianaq/Speedy/Projects/gaurdianaq-portfolio/.stack-work/install/x86_64-linux-tinfo6/7b0d1666fd5fd3d1c51c5b7c84923da3f6efa3d802d29dd2918319cf8a902dff/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "stacktest_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "stacktest_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "stacktest_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "stacktest_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "stacktest_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "stacktest_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
