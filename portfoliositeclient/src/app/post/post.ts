export interface Post {
  title: string;
  videoUrl?: string;
  body: string;
}
